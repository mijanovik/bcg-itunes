//
//  AppDelegate.h
//  bcg-itunes
//
//  Created by Nikola Mijanovikj on 10/27/17.
//  Copyright © 2017 Nikola Mijanovik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

